#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

//getopt
#include <getopt.h>

#define WORD unsigned short
#define DWORD unsigned int
#define LONG int

#define BI_RGB        0L
#define BI_RLE8       1L
#define BI_RLE4       2L
#define BI_BITFIELDS  3L
#define BI_JPEG       4L
#define BI_PNG        5L


#pragma pack(push, 1)
typedef struct tagBITMAPFILEHEADER {
    WORD bfType;        // must be 'BM'
    DWORD bfSize;        // size of the whole .bmp file
    WORD bfReserved1;   // must be 0
    WORD bfReserved2;   // must be 0
    DWORD bfOffBits;
} BITMAPFILEHEADER;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct tagBITMAPINFOHEADER {
    DWORD biSize;            // size of the structure
    LONG biWidth;           // image width
    LONG biHeight;          // image height
    WORD biPlanes;          // bitplanes
    WORD biBitCount;         // resolution
    DWORD biCompression;     // compression
    DWORD biSizeImage;       // size of the image
    LONG biXPelsPerMeter;   // pixels per meter X
    LONG biYPelsPerMeter;   // pixels per meter Y
    DWORD biClrUsed;         // colors used
    DWORD biClrImportant;    // important colors
} BITMAPINFOHEADER;
#pragma pack(pop)

typedef struct tagPixels {
    int blue;
    int green;
    int red;
} Pixels;

unsigned char *Boo(unsigned char *rgb, int width, int height, long sizeBmpData);

unsigned char *Max(unsigned char *rgb, int width, int height, long sizeBmpData);

unsigned char *Min(unsigned char *rgb, int width, int height, long sizeBmpData);

Pixels GetRGBValue(unsigned char *rgb, int i, int j, int width);

Pixels CountPixel(Pixels *pPixels, Pixels *pTagPixels);

Pixels MaxPixel(Pixels *pPixels, Pixels *pTagPixels);

Pixels MinPixel(Pixels *pPixels, Pixels *pTagPixels, int i);

unsigned char *SetBoundary(unsigned char *rgb, int width, int height);

unsigned char *Convolute(unsigned char *rgb, int width, int height, long sizeBmpData, float pDouble[3][3]);

unsigned char *Histogram(unsigned char *rgb, int width, int height, long sizeBmpData);

void PowerMatrix2(Pixels *pPixels, float pKernel[3][3], int row, int col, float *pBlue, float *pGreen, float *pRed);

unsigned char Range(float value);

bool GetMatrix(float pMatrix[3][3]);


unsigned char *SetHisrogramRGB(unsigned char *rgb, int width, int height, unsigned char hBlue[256],
                               unsigned char hGreen[256],
                               unsigned char hRed[256]);

unsigned char *ResizeImage(unsigned char *image, int width, int height, int newWidth, int newHeight);

bool MakeThreeImage(unsigned char *image, int width, int height, long sizeBMP, char *string);

unsigned char *LoadBMPImage(int *height, int *width, long *sizeBmpData, const char *input) {

    // declare bitmap structures
    BITMAPFILEHEADER bmpheader;
    BITMAPINFOHEADER bmpinfo;

    FILE *img;
    if ((img = fopen(input, "rb")) == NULL) {
        fprintf(stderr, "Nelze otevrit soubor %s.\n", input);
        perror(input);
        return NULL;
    }

    // Load header bmp
    if (!fread(&bmpheader, sizeof(BITMAPFILEHEADER), 1, img)) {
        fprintf(stderr, "Error, unable load FILEHEADER.\n");
        if (fclose(img) != 0) { perror(input); }
        return NULL;
    }


    if (!fread(&bmpinfo, sizeof(BITMAPINFOHEADER), 1, img)) {
        fprintf(stderr, "Error, unable load FILEHEADER.\n");
        if (fclose(img) != 0) { perror(input); }
        return NULL;
    }


    *height = bmpinfo.biHeight;
    *width = bmpinfo.biWidth;

    // check if file is actually a bmp
    if (bmpheader.bfType != 0x4D42) {
        if (fclose(img) != 0) { perror(input); }
        fprintf(stderr, "Error, not bmp file.\n");
        return NULL;
    }

    // The bitmap is in uncompressed red green blue (RGB) format that is not compressed and does not use color masks.
    // check if bmp is uncompressed
    if (bmpinfo.biCompression != BI_RGB) {
        if (fclose(img) != 0) { perror(input); }
        fprintf(stderr, "Error, not suppoted compression.\n");
        return NULL;
    }

    // check if 24 bit bmp
    if (bmpinfo.biBitCount != 24) {
        if (fclose(img) != 0) { perror(input); }
        fprintf(stderr, "Error, not 24 bit.\n");
        return NULL;
    }

    *sizeBmpData = bmpheader.bfSize - bmpheader.bfOffBits;
    unsigned char *buffer = (unsigned char *) malloc((size_t) *sizeBmpData);

    if (buffer == NULL) {
        fprintf(stderr, "Error, malloc buffer.\n");
        if (fclose(img) != 0) { perror(input); }
    }

    // move file pointer to the begining of bitmap data
    if (fseek(img, bmpheader.bfOffBits, SEEK_SET) != 0) { perror("Fseek"); }

    if (!fread(buffer, (size_t) *sizeBmpData, 1, img)) {
        if (fclose(img) != 0) { perror(input); }
        free(buffer);
        fprintf(stderr, "Error, load bmp data.\n");
        return NULL;
    }

    if (fclose(img) != 0) { perror(input); }
    return buffer;
}


bool WriteBMPImage(unsigned char *bmp, int width, int height, long sizeBMPData, const char *filenameOut) {

    if (bmp == NULL) {
        fprintf(stderr, "NULL writing image\n");
        return EXIT_FAILURE;
    }

    // declare bmp structures
    BITMAPFILEHEADER bmfh;
    BITMAPINFOHEADER info;

    // andinitialize them to zero
    memset(&bmfh, 0, sizeof(BITMAPFILEHEADER));
    memset(&info, 0, sizeof(BITMAPINFOHEADER));

    // fill the fileheader with data
    bmfh.bfType = 0x4d42;       // 0x4d42 = 'BM'
    bmfh.bfReserved1 = 0;
    bmfh.bfReserved2 = 0;
    bmfh.bfSize = (unsigned int) (sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + sizeBMPData);
    bmfh.bfOffBits = 0x36;        // number of bytes to start of bitmap bits

    // fill the infoheader

    info.biSize = sizeof(BITMAPINFOHEADER);
    info.biWidth = width;
    info.biHeight = height;
    info.biPlanes = 1;            // we only have one bitplane
    info.biBitCount = 24;        // RGB mode is 24 bits
    info.biCompression = BI_RGB;
    info.biSizeImage = 0;        // can be 0 for 24 bit images
    info.biXPelsPerMeter = 0x0ec4;     // paint and PSP use this values
    info.biYPelsPerMeter = 0x0ec4;
    info.biClrUsed = 0;            // we are in RGB mode and have no palette
    info.biClrImportant = 0;    // all colors are important


    FILE *out;

    if ((out = fopen(filenameOut, "wb")) == NULL) {

        fprintf(stderr, "Error, nelze otevrit soubor %s.\n", filenameOut);
        return EXIT_FAILURE;
    }

    if (!fwrite(&bmfh, sizeof(BITMAPFILEHEADER), 1, out)) {
        fprintf(stderr, "Error, fwrite fileHeader.\n");
        if (fclose(out) != 0) { perror(filenameOut); }
        return EXIT_FAILURE;
    }

    if (!fwrite(&info, sizeof(BITMAPINFOHEADER), 1, out)) {
        fprintf(stderr, "Error, fwrite infoHeader.\n");
        if (fclose(out) != 0) { perror(filenameOut); }
        return EXIT_FAILURE;
    }

    if (!fwrite(bmp, (size_t) sizeBMPData, 1, out)) {
        fprintf(stderr, "Error, fwrite data image.\n");
        if (fclose(out) != 0) { perror(filenameOut); }
        return EXIT_FAILURE;
    }

    if (fclose(out) != 0) {
        perror(filenameOut);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}


void PrintUsage(char *string) {
    printf("Usage: %s -i image -o directory [-c num -g ]", string);
}

int main(int argc, char *argv[]) {

    int c, iflg = 0, oflg = 0, cflg = 0, rflg = 0, gflg = 0;
    char *ivalue = NULL;
    char *ovalue = NULL;
    char *cvalue = NULL;

    while ((c = getopt(argc, argv, "i:o:c:rg")) != -1) {
        switch (c) {
            case 'i':
                ivalue = optarg;
                iflg++;
                printf("ivalue%s\n", ivalue);
                break;
            case 'o':
                ovalue = optarg;
                oflg++;
                printf("ovalue%s\n", ovalue);
                break;
            case 'c':
                cvalue = optarg;
                cflg++;
                printf("cvalue%s\n", cvalue);
                break;
            case 'r':
                rflg++;
                break;
            case 'g':
                gflg++;
                break;

            default:
                PrintUsage(argv[0]);
                return EXIT_FAILURE;
        }
    }

    if (argc < 5 || argc > 7) {
        PrintUsage(argv[0]);
        return EXIT_FAILURE;
    }

    if (iflg == 0 || oflg == 0) {
        PrintUsage(argv[0]);
        return EXIT_FAILURE;
    }


    int height, width;
    long sizeBMP;

    unsigned char *originalImage = LoadBMPImage(&height, &width, &sizeBMP, ivalue);
    if (originalImage == NULL) {
        perror("Load BMP image. ");
        return EXIT_FAILURE;
    }

    // jenom Avg, Min, MAx
    if (argc == 5) {
        if (MakeThreeImage(originalImage, width, height, sizeBMP, ovalue) != 0) {
            free(originalImage);
            return EXIT_FAILURE;
        }
    }
    else if (argc == 6 && rflg != 0) {
        int newWidth, newHeight;

        if (scanf("%d %d", &newWidth, &newHeight) != 2) {
            fprintf(stderr, "Error, input data.\n");
            free(originalImage);
            return EXIT_FAILURE;
        }

        unsigned char *outBPM = ResizeImage(originalImage, width, height, newWidth, newHeight);
        width = newWidth;
        height = newHeight;
        sizeBMP = width * height * 3;

        char dir[1000];
        strcpy(dir, ovalue);
        strcat(dir, "resize.bmp");

        if (WriteBMPImage(outBPM, width, height, sizeBMP, dir)) {
            fprintf(stderr, "Error, write bmp image %s.\n", dir);
            free(originalImage);
            free(outBPM);
            return EXIT_FAILURE;
        }
        free(outBPM);
    }

    else if (argc == 6 && gflg != 0) {
        unsigned char *outBPM = Histogram(originalImage, width, height, sizeBMP);
        char dir[1000];
        strcpy(dir, ovalue);
        strcat(dir, "histogram.bmp");

        if (WriteBMPImage(outBPM, width, height, sizeBMP, dir)) {
            fprintf(stderr, "Error, write bmp image %s.\n", dir);
            free(originalImage);
            free(outBPM);
            return EXIT_FAILURE;
        }
        free(outBPM);
    }
    else if (argc == 7 && cflg != 0) {
        int num = atoi(cvalue);
        if (num <= 0 || num > 7) {
            fprintf(stderr, "Range 1 -7");
            free(originalImage);
            return EXIT_FAILURE;
        }

        char dir[1000];
        strcpy(dir, ovalue);
        strcat(dir, "convolute.bmp");

        unsigned char *outBPM = (unsigned char *) malloc((size_t) sizeBMP);
        if (outBPM == NULL) {
            fprintf(stderr, "Error, malloc buffer.\n");
            free(originalImage);
            return EXIT_FAILURE;
        }

        if (num == 1) {
            //relief
            float kernel[3][3] = {{-2, -1, 0},
                                  {-1, 1,  1},
                                  {0,  1,  2}};
            outBPM = Convolute(originalImage, width, height, sizeBMP, kernel);
        }
        else if (num == 2) {
            //detekce hran
            float kernel[3][3] = {{0, 1,  0},
                                  {1, -4, 1},
                                  {0, 1,  0}};
            outBPM = Convolute(originalImage, width, height, sizeBMP, kernel);
        }
        else if (num == 3) {
            //zaostreni
            float kernel[3][3] = {{0,  -1, 0},
                                  {-1, 5,  -1},
                                  {0,  -1, 0}};
            outBPM = Convolute(originalImage, width, height, sizeBMP, kernel);
        }
        else if (num == 4) {
            //obrys
            float kernel[3][3] = {{-1, -1, -1},
                                  {-1, 8,  -1},
                                  {-1, -1, -1}};
            outBPM = Convolute(originalImage, width, height, sizeBMP, kernel);
        }
        else if (num == 5) {
            // zaostreni 2
            float kernel[3][3] = {{-1, -1, -1},
                                  {-1, 9,  -1},
                                  {-1, -1, -1}};
            outBPM = Convolute(originalImage, width, height, sizeBMP, kernel);
        }
        else if (num == 6) {
            // blur, rozmazani
            float kernel[3][3] = {
                    {1 / 9.0, 1 / 9.0, 1 / 9.0},
                    {1 / 9.0, 1 / 9.0, 1 / 9.0},
                    {1 / 9.0, 1 / 9.0, 1 / 9.0}
            };

            outBPM = Convolute(originalImage, width, height, sizeBMP, kernel);
        }
        else if (num == 7) {
            float kernel[3][3] = {{0, 0, 0},
                                  {0, 0, 0},
                                  {0, 0, 0}};

            if (GetMatrix(kernel)) {
                free(originalImage);
                return EXIT_FAILURE;
            }
            outBPM = Convolute(originalImage, width, height, sizeBMP, kernel);
        }
        if (WriteBMPImage(outBPM, width, height, sizeBMP, dir)) {
            fprintf(stderr, "Error, write bmp image %s.\n", dir);
            free(outBPM);
            return EXIT_FAILURE;
        }
        free(outBPM);
    }
    else {
        fprintf(stderr, "Someting parameter is wrong.");
    }


    free(originalImage);
    return 0;
}

bool MakeThreeImage(unsigned char *image, int width, int height, long sizeBMP, char *string) {

    unsigned char *outBPM = Boo(image, width, height, sizeBMP);

    char dir[1000];
    strcpy(dir, string);
    strcat(dir, "avg.bmp");

    if (WriteBMPImage(outBPM, width, height, sizeBMP, dir)) {
        fprintf(stderr, "Error, write bmp image %s.\n", dir);
        free(outBPM);
        return EXIT_FAILURE;
    }

    memset(dir, 0, sizeof(dir));
    strcpy(dir, string);
    strcat(dir, "max.bmp");

    outBPM = Max(image, width, height, sizeBMP);

    if (WriteBMPImage(outBPM, width, height, sizeBMP, dir)) {
        fprintf(stderr, "Error, write bmp image %s.\n", dir);
        free(outBPM);
        return EXIT_FAILURE;
    }

    memset(dir, 0, sizeof(dir));
    strcpy(dir, string);
    strcat(dir, "min.bmp");

    outBPM = Min(image, width, height, sizeBMP);

    if (WriteBMPImage(outBPM, width, height, sizeBMP, dir)) {
        fprintf(stderr, "Error, write bmp image %s.\n", dir);
        free(outBPM);
        return EXIT_FAILURE;
    }


    free(outBPM);
    return EXIT_SUCCESS;
}

unsigned char *ResizeImage(unsigned char *image, int width, int height, int newWidth, int newHeight) {
    /*METODA Nearest-neighbor interpolation*/

    unsigned char *buffer = (unsigned char *) malloc(newWidth * newHeight * 3);
    if (buffer == NULL) {
        fprintf(stderr, "Error, malloc buffer.\n");
        return NULL;
    }

    double scaleWidth = (double) newWidth / (double) width;
    double scaleHeight = (double) newHeight / (double) height;
    printf("%g:%g\n", scaleWidth, scaleHeight);

    int index;
    for (int j = 0; j < newHeight; ++j) {
        for (int i = 0; i < newWidth; ++i) {
            index = (((int) (i / scaleHeight) * (width * 3)) + ((int) (j / scaleWidth) * 3));
            buffer[i * newWidth * 3 + j * 3] = image[index];
            buffer[i * newWidth * 3 + j * 3 + 1] = image[index + 1];
            buffer[i * newWidth * 3 + j * 3 + 2] = image[index + 2];
        }
    }

    return buffer;
}

bool GetMatrix(float pMatrix[3][3]) {
    float num;
    printf("Enter values.\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("M%d%d = ", i + 1, j + 1);
            if ((scanf("%f", &num)) != 1) {
                fflush(stdin);
                fprintf(stderr, "Unsuported value. Try again.\n");
                return EXIT_FAILURE;
            }
            pMatrix[i][j] = num;
        }
    }

    printf("\nSample matrix\n");
    for (int x = 0; x < 3; x++) {
        for (int y = 0; y < 3; y++) {
            printf("%f ", pMatrix[x][y]);
        }
        printf("\n");
    }
    return EXIT_SUCCESS;
}

unsigned char *Boo(unsigned char *rgb, int width, int height, long sizeBmpData) {

    unsigned char *buffer = (unsigned char *) malloc((size_t) sizeBmpData);
    if (buffer == NULL) {
        fprintf(stderr, "Error, malloc buffer.\n");
        return NULL;
    }

    Pixels px = {0, 0, 0};
    for (int i = 3; i < 3 * width - 3; i += 3) {
        for (int j = 3; j < 3 * height - 3; j += 3) {

            Pixels pixel = {0, 0, 0};
            for (int k = -3; k <= 3; k += 3) {
                for (int m = -3; m <= 3; m += 3) {
                    px = GetRGBValue(rgb, i + k, j + m, width);
                    pixel = CountPixel(&px, &pixel);
                }
            }

            buffer[i * width + j] = (unsigned char) (pixel.blue / 9);
            buffer[i * width + j + 1] = (unsigned char) (pixel.green / 9);
            buffer[i * width + j + 2] = (unsigned char) (pixel.red / 9);
        }
    }

    //return SetBoundary(buffer, width, height);
    return buffer;

}

unsigned char *Max(unsigned char *rgb, int width, int height, long sizeBmpData) {
    unsigned char *buffer = (unsigned char *) malloc((size_t) sizeBmpData);
    if (buffer == NULL) {
        fprintf(stderr, "Error, malloc buffer.\n");
        return NULL;
    }

    Pixels px = {0, 0, 0};
    Pixels pixel;
    for (int i = 3; i < 3 * width - 3; i += 3) {
        for (int j = 3; j < 3 * height - 3; j += 3) {

            pixel.blue = 0;
            pixel.green = 0;
            pixel.red = 0;

            for (int k = -3; k <= 3; k += 3) {
                for (int m = -3; m <= 3; m += 3) {
                    px = GetRGBValue(rgb, i + k, j + m, width);
                    pixel = MaxPixel(&px, &pixel);
                }
            }
            buffer[i * width + j] = (unsigned char) (pixel.blue);
            buffer[i * width + j + 1] = (unsigned char) (pixel.green);
            buffer[i * width + j + 2] = (unsigned char) (pixel.red);
        }
    }
    //return SetBoundary(buffer, width, height);
    return buffer;
}

unsigned char *Min(unsigned char *rgb, int width, int height, long sizeBmpData) {
    unsigned char *buffer = (unsigned char *) malloc((size_t) sizeBmpData);
    if (buffer == NULL) {
        fprintf(stderr, "Error, malloc buffer.\n");
        return NULL;
    }
    Pixels px = {0, 0, 0};
    for (int i = 3; i < 3 * width - 3; i += 3) {
        for (int j = 3; j < 3 * height - 3; j += 3) {

            Pixels pixel = {255, 255, 255};
            for (int k = -3; k <= 3; k += 3) {
                for (int m = -3; m <= 3; m += 3) {
                    px = GetRGBValue(rgb, i + k, j + m, width);
                    pixel = MinPixel(&px, &pixel, k);
                }
            }
            buffer[i * width + j] = (unsigned char) (pixel.blue);
            buffer[i * width + j + 1] = (unsigned char) (pixel.green);
            buffer[i * width + j + 2] = (unsigned char) (pixel.red);
        }
    }
    //return SetBoundary(buffer, width, height);
    return buffer;
}

Pixels MaxPixel(Pixels *A, Pixels *B) {
    Pixels result;

    if ((A->blue + A->red + A->green) >= (B->green + B->red + B->blue)) {
        result.blue = A->blue;
        result.red = A->red;
        result.green = A->green;
    }
    else {
        result.blue = B->blue;
        result.red = B->red;
        result.green = B->green;
    }

    return result;
}

Pixels MinPixel(Pixels *A, Pixels *B, int i) {
    Pixels result;

    if (i == -3) {
        result.blue = A->blue;
        result.red = A->red;
        result.green = A->green;
    }
    else if ((A->blue + A->red + A->green) <= (B->green + B->red + B->blue)) {
        result.blue = A->blue;
        result.red = A->red;
        result.green = A->green;
    }
    else {
        result.blue = B->blue;
        result.red = B->red;
        result.green = B->green;
    }

    return result;
}

Pixels CountPixel(Pixels *pPixels, Pixels *pTagPixels) {
    Pixels result;
    result.blue = pTagPixels->blue + pPixels->blue;
    result.green = pTagPixels->green + pPixels->green;
    result.red = pTagPixels->red + pPixels->red;

    return result;
}

Pixels GetRGBValue(unsigned char *rgb, int i, int j, int width) {
    Pixels result;
    result.blue = rgb[i * width + j];
    result.green = rgb[i * width + j + 1];
    result.red = rgb[i * width + j + 2];
    return result;
}

unsigned char *SetBoundary(unsigned char *rgb, int width, int height) {
    int i;
    for (i = 0; i < 3 * width; i += 3) {
        rgb[i * width] = 255;
        rgb[i * width + 3 * height - 3] = 255;
    }

    for (int j = 0; j < 3 * height; j += 3) {
        rgb[j] = 255;
        rgb[(i - 3) * width + j] = 255;
    }
    return rgb;
}

unsigned char *Convolute(unsigned char *rgb, int width, int height, long sizeBmpData, float Kernel
[3][3]) {

    unsigned char *buffer = (unsigned char *) malloc((size_t) sizeBmpData);
    if (buffer == NULL) {
        fprintf(stderr, "Error, malloc buffer.\n");
        return NULL;
    }
    Pixels px = {0, 0, 0};
    float valueBlue, valueGreen, valueRed;
    for (int i = 3; i < 3 * width - 3; i += 3) {
        for (int j = 3; j < 3 * height - 3; j += 3) {
            valueRed = 0, valueBlue = 0, valueGreen = 0;
            //Pixels pixel = {0, 0, 0};
            for (int k = -3, row = 0; k <= 3; k += 3, row++) {
                for (int m = -3, col = 0; m <= 3; m += 3, col++) {
                    px = GetRGBValue(rgb, i + k, j + m, width);
                    valueBlue += px.blue * Kernel[row][col];
                    valueGreen += px.green * Kernel[row][col];
                    valueRed += px.red * Kernel[row][col];
                }
            }

            buffer[i * width + j] = Range(valueBlue);
            buffer[i * width + j + 1] = Range(valueGreen);
            buffer[i * width + j + 2] = Range(valueRed);
        }
    }
    //return SetBoundary(buffer, width, height);
    return buffer;
}

unsigned char Range(float value) {
    if (value >= 255) { return 255; }
    if (value <= 0) { return 0; }
    return (unsigned char) value;
}

unsigned char *Histogram(unsigned char *rgb, int width, int height, long sizeBmpData) {

    if (width < 512 || height < 512) {
        fprintf(stderr, "Histogram: requested image size bigger then 511x511");
        return NULL;
    }

    unsigned char *buffer = (unsigned char *) malloc((size_t) sizeBmpData);
    if (buffer == NULL) {
        fprintf(stderr, "Error, malloc buffer.\n");
        return NULL;
    }

    unsigned char hBlue[256] = {0};
    unsigned char hGreen[256] = {0};
    unsigned char hRed[256] = {0};

    Pixels px = {0, 0, 0};
    for (int i = 0; i < 3 * width; i += 3) {
        for (int j = 0; j < 3 * height; j += 3) {
            px = GetRGBValue(rgb, i, j, width);
            hBlue[px.blue]++;
            hGreen[px.green]++;
            hRed[px.red]++;
        }
    }

    return SetHisrogramRGB(rgb, width, height, hBlue, hGreen, hRed);
}

unsigned char *SetHisrogramRGB(unsigned char *rgb, int width, int height, unsigned char hBlue[256],
                               unsigned char hGreen[256], unsigned char hRed[256]) {

    for (int i = 0, k = 0; i < 3 * 256; i += 3, k++) {
        for (int j = 42 * 3, m = 0; m <= hBlue[k] / 2; j += 3, m++) {
            rgb[j * width + i] = 255;
            rgb[j * width + i + 1] = 0;
            rgb[j * width + i + 2] = 0;
        }

        for (int j = 42 * 3 + 384 + 42 * 3, m = 0; m <= hGreen[k] / 2; j += 3, m++) {
            rgb[j * width + i] = 0;
            rgb[j * width + i + 1] = 255;
            rgb[j * width + i + 2] = 0;
        }

        for (int j = 42 * 3 + 384 + 42 * 3 + 384 + 42 * 3, m = 0; m <= hRed[k] / 2; j += 3, m++) {
            rgb[j * width + i] = 0;
            rgb[j * width + i + 1] = 0;
            rgb[j * width + i + 2] = 255;
        }
    }
    return rgb;
}


