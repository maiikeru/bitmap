##
# Projekt: BMP
# Branch:  Master
# Autor:   Michal Jurca
# Datum:   23.9.2015  
# Změna:   23.9.2015  
# Použití:
#	-překlad     make
#	-smazat      make clean
#	-zabalit     make pack                  
##  

###############################################################################
#  Parameters
###############################################################################

# Jmeno archivu a soubory
PACK_NAME := xjurca07

#Smazani souboru
RM = rm -f

#Compilator	
CPP:=gcc

# Optimalizace
OPT = -g -O3 

# Parametry a compilator pro C++
FLAGS =-std=c99 -Wall -Werror -pedantic-errors $(OPT)
#FLAGS =  -std=c++11 

# Zdrojový soubor
SRCS = $(wildcard *.c)

# Objektový soubor
OBJFILES = $(SRCS:.c=.o)

# Spustitelný soubor
EXEFILE:= main

# Obsah projektu
ALLFILES =  Makefile $(SRCS) 


###############################################################################
#  Compilation
###############################################################################

.PHONY: clean pack 

all: $(EXEFILE)
		
$(EXEFILE): $(SRCS)
	$(CPP) $(FLAGS) $< -o $@
###############################################################################
#  Cleaning and packing
###############################################################################

clean:
	$(RM) $(OBJFILES) $(EXEFILE) *~

# Vytvoreni xjurca07.tar
pack:
	tar -cf $(PACK_NAME).tar $(ALLFILES)


#debug: $(EXEFILE)
#	export XEDITOR=gvim;ddd $(EXEFILE)


run: $(EXEFILE)
	./$(EXEFILE)

# End of Makefile
